package p2pEsPeTaC;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import net.jxta.peergroup.PeerGroup;
import net.jxta.share.ContentAdvertisement;
import net.jxta.share.client.CachedListContentRequest;

//This class will search for specifed contents through SaEeD Group
//inner class for search
public class SearchFile extends Thread {
    //Defining Class Variables

    private JTextArea log = null;
    private PeerGroup EsPeTaCGroup = null;
    private String searchValue = null;
    protected ListRequestor requestor = null;
    private JTable table = null;
    public static ContentAdvertisement[] contents = null;
    private boolean running = true;

    public SearchFile(PeerGroup group, String searchKey, JTextArea log, JTable table) {
        this.EsPeTaCGroup = group;
        this.searchValue = searchKey;
        this.log = log;
        this.table = table;

    }

    private void printOnLog(final String toPrint) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(toPrint);
            }
        });
    }

    @Override
    public void run() //cause this thread to execute as long as needed to find 
    {                 // the Contents  
        int i = 0;
        while (i < 7 && running) {
            if (requestor != null && requestor.getResults() != null) {
                break;
            }
            requestor = new ListRequestor(EsPeTaCGroup, searchValue, log, table);
            requestor.activateRequest();
            try {
                Thread.sleep(2 * 1000); //Time out for each search through network
                i++;
            } catch (InterruptedException ex) {
                Logger.getLogger(SearchFile.class.getName()).log(Level.SEVERE, null, ex);
                stopThread();
            }
        }
        printOnLog("[-]A procura por arquivos foi terminada.\n");
        stopThread();
    }

    public void stopThread() //This method will stop search Process
    {
        running = false;
        if (requestor != null) {
            requestor.cancel();
        }
    }

    public void killThread() //This method will Terminate the Search Thread
    {
        printOnLog("[-]Thread de procura parada.\n");
        running = false;
    }

    public ContentAdvertisement[] getContentAdvs() //Accessor to show found contents
    {
        return requestor.searchResult;
    }
}

class ListRequestor extends CachedListContentRequest {

    public static ContentAdvertisement[] searchResult = null;
    private JTextArea log = null;
    private JTable table = null;

    public ListRequestor(PeerGroup EsPeTaCGroup, String SubStr, JTextArea log, JTable table) {
        super(EsPeTaCGroup, SubStr);
        this.log = log;
        this.table = table;
    }

    @Override
    public void notifyMoreResults() //this method will notify user when new contents are found
    {
        printOnLog("[+]Procurando por mais conteudos.\n");
        searchResult = getResults();
        //showing the results
        String[] titles = {"Nome do arquivo", "Tamanho(Bytes)", "Check Sum (CRC-32)"};
        //add new contents to Search table
        DefaultTableModel TableModel1 = new DefaultTableModel(titles, searchResult.length);
        table.setModel(TableModel1);


        printOnLog("[*]Encontrado: " + searchResult[0].getName() + "\n"
                + "Tamanho: " + searchResult[0].getLength() + " Bytes\n\n");
        table.setValueAt(searchResult[0].getName(), 0, 0);
        table.setValueAt(searchResult[0].getLength(), 0, 1);
        table.setValueAt(searchResult[0].getDescription(), 0, 2);

    }

    public ContentAdvertisement[] getContentAdvs()//acessor to return contents
    {
        return searchResult;
    }

    private void printOnLog(final String toPrint) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(toPrint);
            }
        });
    }
}
