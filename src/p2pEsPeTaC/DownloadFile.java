package p2pEsPeTaC;

import java.io.File;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import net.jxta.peergroup.PeerGroup;
import net.jxta.share.ContentAdvertisement;
import net.jxta.share.client.GetContentRequest;

//This class runs as Thread and start Downloading the File as soon as called
//inner class which handles download requestes
public class DownloadFile extends Thread {

    
    protected GetRemoteFile myDownloader = null;
    private JTextArea log;

    private void printOnLog(final String toPrint) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(toPrint);
            }
        });
    }

    public DownloadFile(PeerGroup group, ContentAdvertisement contentAdv, File destination, JTextArea log,
            JProgressBar progress) {
        this.log = log;
        printOnLog("[+]Iniciando Download do Objeto.\n");
        //inner classes used here for better performance
        myDownloader = new GetRemoteFile(group, contentAdv, destination, this.log, progress);

    }
}

class GetRemoteFile extends GetContentRequest {

    private JProgressBar progressBar = null;
    private JTextArea log = null;
    

    public GetRemoteFile(PeerGroup group, ContentAdvertisement contentAdv, File destination, JTextArea log,
            JProgressBar progress) {
        super(group, contentAdv, destination);
        this.progressBar = progress;
        this.log = log;

        printOnLog("[+]Download em Progresso.\n");
    }

    @Override
    public void notifyUpdate(int percentage) //this method will notify about download progress
    {
        progressBar.setValue(percentage);
    }

    @Override
    public void notifyDone()//this method will return message about download process 
    {
        printOnLog("[+]Donwloading -> Sucesso. o/ \n");
    }

    @Override
    public void notifyFailure()//this method will return message if download failed 
    {
        printOnLog("[-] O download falhou devido a uma exceção no sistema! ç.ç ");
    }

    private void printOnLog(final String toPrint) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(toPrint);
            }
        });
    }
}
