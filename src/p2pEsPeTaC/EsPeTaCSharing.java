package p2pEsPeTaC;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import net.jxta.exception.PeerGroupException;
import net.jxta.peergroup.PeerGroup;
import net.jxta.share.CMS;
import net.jxta.share.Content;
import net.jxta.share.ContentManager;
import net.jxta.share.SearchListener;

/**
 * Esta classe irá compartilhar o conteúdo através dos peers dentro do Grupo
 * EsPeTaC
 *
 * @author pedrotanaka
 */
public class EsPeTaCSharing extends Thread implements SearchListener {
    //Defining Class Variables

    private PeerGroup EsPeTaCGroup = null;
    private JTextArea log = null;
    private File myPath = null;
    //using Content Management Service Library for Sharing purposes
    private CMS cms = null;

    /**
     * Creates a new instance of SaEeDSharing
     *
     */
    public EsPeTaCSharing(PeerGroup group, JTextArea log, File givenPath) {
        this.log = log;
        this.EsPeTaCGroup = group;
        this.myPath = givenPath;
        launchCMS();
    }

    private void printOnLog(final String textToPrint) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(textToPrint);
            }
        });
    }

    
    
    private void launchCMS() {
        //This method will initializie the CMS library

        printOnLog("[+]Inicializando as Bibliotecas CMS...\n");
        cms = new CMS();
        try {
            cms.init(EsPeTaCGroup, null, null);//binding CMS object to SaEeD Group
            if (cms.startApp(myPath) == -1) {
                printOnLog("[-]Criação do Objeto CMS Falhou.\n");
                printOnLog("[!]Inicialização do CMS Falhou.\nSaindo.");
                System.exit(-1);
            } else {
                printOnLog("[+]Objeto do CMS criado com sucesso.\n");
            }
            File temp = new File(myPath.getAbsolutePath() + File.separator + "shares.ser");
            if(temp.exists()){
                temp.delete();
            }
            //sharing all files in shared directory
            ContentManager contentManager = null;
            contentManager = cms.getContentManager();
            File[] list = myPath.listFiles();
            Arrays.sort(
                    list,
                    new Comparator<File>() {
                        @Override
                        public int compare(File a, File b) {
                            return a.getName().compareTo(b.getName());
                        }
                    });
            CheckSumCalc checkSum = new CheckSumCalc();


            for (int i = 0; i < list.length; i++) {
                if (list[i].isFile()) {//Sharing Files and check sums in network
                    contentManager.share(list[i], checkSum.getFileSum(list[i]));
                }
            }
            printOnLog("======= Conteúdo compartilhado =======\n");
            //viewing the shared contents
            Content[] content = cms.getContentManager().getContent();
            //also shows the share contents in log area
            for (int j = 0; j < content.length; j++) {
                printOnLog("[*]" + content[j].getContentAdvertisement().getName() + "\tCheck Sum: "
                        + content[j].getContentAdvertisement().getDescription() + "\n");
            }
            printOnLog("[+]Todo conteúdo foi compartilhado com sucesso :-)\n");

        } catch (PeerGroupException ex) {
            printOnLog("[!]Inicialização do CMS Falhou.\nSaindo.");
            ex.printStackTrace();
            System.exit(-1);
        } catch (IOException e) {
            printOnLog("[-]Exceção: " + e.getMessage() + "\n[!]Certifique que o arquivo: \"Shares.ser\" está apagado antes "
                    + " de iniciar o serviço.\n");
            printOnLog("[-]Exceção: " + e.getMessage());
        }

        printOnLog("[===========================]\n");

    }

    public void stopCMS()//this method will stop Content management Service
    {
        printOnLog("[+]Parando o Objeto CMS .\n");
        cms.stopApp();
        printOnLog("[+]Deletando o arquivo de anúncios de conteúdo CMS.\n");
        File temp = new File(myPath.getAbsolutePath() + File.separator + "shares.ser");
        if (temp.delete()) {   //also deletes the CMS data file
            printOnLog("[+]Arquivo \"" + myPath.getAbsolutePath() + File.separator + "shares.ser\" deletado com sucesso.\n");
            printOnLog("[+]Arquivo shares.ser deletado com sucesso.");
        } else {
            printOnLog("[-]Arquivo shares.ser não encontrado!\n");
        }
    }
    //Listener to shows requested queries

    @Override
    public void queryReceived(String query) {
        printOnLog("[Query recebida]: " + query);
    }
}